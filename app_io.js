// http://appium.io/docs/en/about-appium/api/
"use strict";

const wdio = require('webdriverio');
const assert = require('chai');
const WEBVIEW_CONTEXT = 'WEBVIEW_com.example.app'

describe("App Webview", function() {
    describe('Guess number', function() {
        it("Should validate page title", () => {
            let contexts = browser.contexts()
            browser.context(WEBVIEW_CONTEXT)

            let title = browser.getText("~Appium Labs Webview")
            console.log(title)
            title.should.be.equal('MyGuessApp')
        })
    });
})