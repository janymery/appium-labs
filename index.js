"use strict"

require("./helpers/setup")
const wdio = require("webdriverio")

const capabilities = {
  port: 4723,
  desiredCapabilities: {
    platformName: "Android",
    platformVersion: "7.0",
    deviceName: "Galaxy Nexus API 24",
    app: "/Users/janemery/dev/appium-labs/webview-app/app/build/outputs/apk/debug/app-debug.apk",
    automationName: "UiAutomator2"
  }
};

describe("test webview", function () {
  this.timeout(300000)
  let driver

  before(function () {
    driver = wdio.remote(capabilities)
    return driver.init()
  })

  it("should find elements", function () {
    return driver.contexts().then(function (contexts) { // get list of available views. Returns array: ["NATIVE_APP","WEBVIEW_1"]
        console.log(contexts)
        return driver.context('WEBVIEW_com.example.app')
    }).elementById('input_my_guess')
      .setValue(12)
      .end()
      .catch(function(err) {
          console.log(err);
      })
    })    
})
