// http://appium.io/docs/en/about-appium/api/
"use strict";

require("./helpers/setup");

var wd = require("wd")

const WEBVIEW_CONTEXT = 'WEBVIEW_com.example.app'

const desiredCapabilities = {
    platformName: "Android",
    platformVersion: "7.0",
    deviceName: "Galaxy Nexus API 24",
    app: "/Users/janemery/dev/appium-labs/webview-app/app/build/outputs/apk/debug/app-debug.apk",
    automationName: "UiAutomator2",
    showChromedriverLog: true
};

describe("App Webview", function () {
  this.timeout(300000);
  var driver;

  before(function () {
    driver = wd.promiseChainRemote({
      host: '127.0.0.1',
      port: 4723
    });
    return driver
      .init(desiredCapabilities)
      .setImplicitWaitTimeout(3000);
  });

  after(function () {
    return driver
      .quit()
      .finally(function () {
        // some teardown function here        
      });
  });

  describe('Guess number', function() {
    it("Should validate page title", async () => {
      let ctx = await driver.contexts()
      await driver.context(WEBVIEW_CONTEXT)
      
      let title = await driver.title()
      title.should.be.equal('MyGuessApp')
    })

    it("Should visualize game results", async () => {
      let ctx = await driver.contexts()
      await driver.context(WEBVIEW_CONTEXT)
      
      await driver.elementById('input_my_guess')
        .sendKeys('12')
        .elementById('btn_result')
        .click()

      let result = await driver.elementById('result_label').getAttribute("innerText")
      result.should.contains('Segredo era:')    
    })
  })

  describe('Add Patient', function() {
    it("Should have header", async () => {
      await driver.context(WEBVIEW_CONTEXT)

      let subtitle = await driver.elementById('titulo-form').getAttribute("innerText")
      subtitle.should.contains('Adicionar novo paciente')
    })

    it("Should save patient info", async () => {
      let ctx = await driver.contexts()
      await driver.context(WEBVIEW_CONTEXT)

      //let table = await driver.elementByAccessibilityId("Meus pacientes");
      //console.log(table)
    })

    it("Should save patient info", async () => {
      let ctx = await driver.contexts()
      await driver.context(WEBVIEW_CONTEXT)

      let name = await driver.elementById('nome')
      let weigth = await driver.elementById('peso')
      let height = await driver.elementById('altura')
      let fat = await driver.elementById('gordura')

      await name.sendKeys('Maria')
      await weigth.sendKeys('100')
      await height.sendKeys('1.90')
      await fat.sendKeys('35')

      await driver.elementById('adicionar-paciente').click()

      let addedPatient = await driver.elementById('tabela-pacientes').getAttribute("innerText")
      addedPatient.should.contains('Maria')
    })
  })  
});