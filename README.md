# Testes de apps hibridas

1 - Mudar contexto do app no webdriver
- http://appium.io/docs/en/writing-running-appium/web/hybrid/

2 - Verificar a versão do chromedriver ao realizar teste
- https://github.com/appium/appium/blob/master/docs/en/writing-running-appium/web/chromedriver.md

A partir do momento que o contexto é alterado, todos os comandos de manipulação de elementos, serão referentes aquele contexto. 

No caso do webview, torna-se um cenário de testes de app web comum.

## Mudando contexto do App

Para manipular elementos em uma webview, o Appium requer que o contexto do teste seja mudado para esse webview.

Para obter a lista de contextos, usar:
> http://appium.io/docs/en/commands/context/get-contexts/index.html

```javascript
// webdriver.io example
let contexts = driver.contexts();

// wd example
let contexts = await driver.contexts();
```

resultado de exemplo:
```sh
[ 'NATIVE_APP',
  'WEBVIEW_com.example.app'
  'WEBVIEW_chrome' ]
```

Para definir o contexto desejado, basta passar o nome do contexto como parametro:

```javascript
let ctx = await driver.contexts()
console.log(ctx)
await driver.context('WEBVIEW_com.example.app')
```

Para retornar ao contexto nativo, utilizar:

```javascript
await driver.context('NATIVE_APP')
```

## Chromedriver

Executar o appium apontando para a versão desejada do chromedriver. Ex:

```sh
$ appium --chromedriver-executable libs/chromedriver-223 --address 127.0.0.1
```